use std::{env, fs::File, io::{Write, BufReader, BufRead}, process::Command};

use prettytable::{Table, row};
use walkdir::WalkDir;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = env::args().skip(1).collect();

    let home_dir = dirs::home_dir().unwrap();
    let file_path = format!("{}/.glc", home_dir.display());

    match args.get(0).map(|s| s.as_str()) {
        Some("config") => config(&args, &file_path).map_err(|e| format!("Couldn't config paths: {}", e))?,
        Some("list") => list(&file_path).map_err(|e| format!("Couldn't list paths: {}", e))?,
        Some("help") => print!("GLC: Git Last Commit
Usage: glc <command> [args]

Commands:
    config:     Configure paths to be tracked.                                  Ex: glc config ~/Path/to/projects_dir/ ~/Path/to/another_projects_dir/
    list:       Shows the location of the git repo and its latest commit msg.   Ex: glc list"),
        Some(s) => println!("Unknown command: {}", s),
        None => list(&file_path).map_err(|e| format!("Couldn't list paths: {}", e))?,
    }

    Ok(())
}

fn config(args: &Vec<String>, file_path: &String) -> Result<(), Box<dyn std::error::Error>> {
    let mut file = File::create(file_path)?;
    file.write_all(args.join("\n").as_bytes())?;

    Ok(())
}

fn list(file_path: &String) -> Result<(), Box<dyn std::error::Error>> {
    let file = File::open(file_path)?;
    let reader = BufReader::new(file);

    let mut lines = Vec::new();

    for line in reader.lines() {
        lines.push(line?);
    }

    let mut paths: Vec<String> = Vec::new();
    let mut msgs: Vec<String> = Vec::new();

    for line in lines {
        for entry in WalkDir::new(line).into_iter().filter_map(|e| e.ok()) {
            if entry.path().join(".git").exists() {
                paths.push(entry.path().display().to_string());

                let output = Command::new("git")
                    .arg("-C")
                    .arg(entry.path())
                    .arg("log")
                    .arg("-1")
                    .arg("--format=%B")
                    .output()
                    .expect("failed to execute process");

                let stdout = String::from_utf8_lossy(&output.stdout);
                let mut msg = stdout.trim().to_string();

                if msg.is_empty() {
                    msg = "Repo has no commits yet".to_string();
                }

                msgs.push(msg);
            }
        }
    }

    let mut table = Table::new();
    table.add_row(row![Fyb => "Git Dir", "Commit Message"]);

    for (i, path) in paths.iter().enumerate() {
        table.add_row(row![Fb -> path, Fg -> msgs[i]]);
    }

    table.printstd();

    Ok(())
}
