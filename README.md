# GLC: Git Last Commit

A simple command-line tool to get the last commit msg of git repositories.

### Usage:
    glc <command> [args]

### Commands:
    config:     Configure paths to be tracked.                                  Ex: glc config ~/Path/to/projects_dir/ ~/Path/to/another_projects_dir/
    list:       Shows the location of the git repo and its latest commit msg.   Ex: glc list
